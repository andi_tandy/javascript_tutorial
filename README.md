# Javascript Tutorial

## Requirements:
* [node.js](https://nodejs.org/en/download/)
* electron
* electron-builder

### install electron using npm package manager:

    npm install electron

### install electron-builder using package manager:

    npm install electron-builder

## How to run:

    npm start

## How to package:

    npm run pack

## Developed using VS Code:
Download VSC [here](https://code.visualstudio.com/#alt-downloads)

Use (**Ctrl+Shift+P**) and enter:

    Git: Clone https://reaper1412@bitbucket.org/reaper1412/javascript_tutorial.git